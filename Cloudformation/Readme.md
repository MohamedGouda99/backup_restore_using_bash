# ECS USING CLOUDFORMATION TEMPLATE

#### This CloudFormation template deploys an Amazon ECS (Elastic Container Service) cluster running on AWS Fargate, a serverless compute engine for containers. It also sets up networking components, security groups, and an Elastic Load Balancer (ELB) to expose a containerized application over the internet. Let's break down the template step by step:

1.	AWSTemplateFormatVersion and Description: Specifies the CloudFormation template version and provides a description of the template.

2.	Parameters: Defines input parameters that can be customized during stack creation.


             •	Stage: A string parameter representing the deployment stage.

             •	ContainerPort: A number parameter specifying the port on which the container listens.

             •	ImageURI: A string parameter representing the URI of the container image.

3.	Resources: Defines the AWS resources to be created.
```       •	Cluster: Creates an ECS cluster.

       •	VPC: Creates a Virtual Private Cloud (VPC).

       •	SubnetA and SubnetB: Create two subnets within the VPC for high availability.

       •	PublicRouteTable and PublicRoute: Set up a public route for internet access.

       •	SubnetAPublicRouteAssociation and SubnetBPublicRouteAssociation: Associate the public route with the subnets.

       •	InternetGateway: Creates an internet gateway.

       •	VPCInternetGatewayAttachment: Attaches the internet gateway to the VPC.

       •	ExecutionRole: Creates an IAM role for ECS tasks.

       •	TaskDefinition: Defines an ECS task definition for running containers.

       •	LoadBalancerSecurityGroup: Creates a security group for the load balancer.

       •	ContainerSecurityGroup: Creates a security group for containers.

       •	LoadBalancer: Creates an Elastic Load Balancer (ELB).

       •	TargetGroup: Creates a target group for the load balancer.

       •	LoadBalancerListener: Creates a listener for the load balancer.

       •	ECSService: Creates an ECS service that uses Fargate to manage containers.

```

4.	DependsOn attributes: Ensures resource creation order, ensuring that dependent resources are created before the resources that depend on them.
5.	Outputs: If there were any outputs, they would be defined here, but they are not present in the provided template.
The template essentially sets up a complete environment for running containerized applications using AWS Fargate and ECS. It includes the ECS cluster, networking components, security groups, and an internet-facing Elastic Load Balancer to distribute incoming traffic to the containers.
When deploying this template, you would provide values for the parameters (Stage, ContainerPort, and ImageURI) and CloudFormation would create the specified resources with the defined configurations.

## How to run cloudformation template ?

### Step1: you can run it do the following script.

```
./create.sh <stack_name> <template_body> <region>
```
### Step2: if you want to update the template do the following.

```
./update.sh <stack_name> <template_body> <region>
```

### Step3: if you want to delete the template do the following:

```
./delete.sh <stack_name> <region>
